# Producer

## The challenge

- https://gitlab.com/alexandreishida/consumer

## The result

### Requirements
- Python 3.4 or newer

### Installation
```bash
$ virtualenv venv
$ source venv/bin/activate
$ pip install -r requirements.txt
```

### Test
```bash
$ PYTHONPATH=src/ pytest -v tests/
```

### Run
```bash
$ PYTHONPATH=src/ python src/main.py http://grupozap-code-challenge.s3-website-us-east-1.amazonaws.com/sources/source-3
```

### Sample of output
```bash
$ PYTHONPATH=src/ python src/main.py http://grupozap-code-challenge.s3-website-us-east-1.amazonaws.com/sources/source-1
Load http://grupozap-code-challenge.s3-website-us-east-1.amazonaws.com/sources/source-1
Send to consumer
Try connection with localhost:4500
................................................................................................................................................................................................................................................................................................................................................................................................................
Sended 400 lines
End
```

### CI
- https://gitlab.com/alexandreishida/producer/pipelines

### Docker run
```bash
$ docker login registry.gitlab.com
$ docker run --rm -it -e SERVER_HOST=172.17.0.1 registry.gitlab.com/alexandreishida/producer:0.1 python src/main.py http://grupozap-code-challenge.s3-website-us-east-1.amazonaws.com/sources/source-3
```

### To Do
- Logging