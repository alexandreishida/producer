import socket

class Client:

    def __init__(self, host, port):
        self.host = host
        self.port = port

    def connect(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect((self.host, self.port))

    def send(self, data):
        return self.sock.sendall(bytes(data, "utf-8"))

    def receive(self):
        return str(self.sock.recv(1), "utf-8")

    def close(self):
        self.sock.close()
