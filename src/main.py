from os import environ
import sys

from client import Client
from producer import Producer

SERVER_HOST = environ.get("SERVER_HOST", "localhost")
SERVER_PORT = int(environ.get("SERVER_PORT", 4500))
FILE_PATH = "data/source"

file_url = None
if len(sys.argv) > 1:
    file_url = sys.argv[1]

if not file_url:
    print("Required file url as parameter")
    print("Sample:")
    print("    PYTHONPATH=src/ python src/main.py http://grupozap-code-challenge.s3-website-us-east-1.amazonaws.com/sources/source-1")
    sys.exit()

client = Client(SERVER_HOST, SERVER_PORT)
producer = Producer(client)

try:
    print("Load {}".format(file_url))
    producer.load(file_url, FILE_PATH)

    print("Send to consumer")
    count = producer.send(FILE_PATH)
    print("Sended {} lines".format(count))

except ConnectionRefusedError:
    print("Connection refused")

print("End")
