import os
import time
import urllib.request

class Producer:

    def __init__(self, client, retry_interval=5):
        self.client = client
        self.retry_interval = retry_interval

    def load(self, file_url, file_path):
        if os.path.exists(file_path):
            os.remove(file_path)

        urllib.request.urlretrieve(file_url, file_path)

    def send(self, file_path, limit = None):
        count = 0

        try:
            print("Try connection with {}:{}".format(self.client.host, self.client.port))
            self.client.connect()

            with open(file_path, "r") as f:
                for line in f:
                    try:
                        # print(line, end="", flush=True)
                        self.client.send(line)
                    except BrokenPipeError:
                        self.retry(line)

                    print(".", end="", flush=True)

                    count += 1
                    if count == limit:
                        break

            print("")
        finally:
            self.client.close()
            pass

        return count

    def retry(self, line):
        self.client.close()

        print("Retry connection", end="", flush=True)
        time.sleep(self.retry_interval)
        self.client.connect()
        self.client.send(line)

