import pytest
from unittest.mock import MagicMock, Mock

from producer import Producer

@pytest.fixture
def producer():
    client = MagicMock()
    producer = Producer(client)

    yield producer

def test_send(producer):
    count = producer.send("data/sample.txt")

    assert count == 3

def test_send_limit(producer):
    count = producer.send("data/sample.txt", 1)

    assert count == 1

def test_retry(producer):
    producer.client.send.side_effect = BrokenPipeError
    producer.retry = Mock()

    count = producer.send("data/sample.txt")

    assert producer.retry.called
